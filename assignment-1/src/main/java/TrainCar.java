public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        if(next == null) return EMPTY_WEIGHT + cat.weight;
        else return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
    }

    public double computeTotalMassIndex() {
        if(next == null) return cat.computeMassIndex();
        return cat.computeMassIndex() + next.computeTotalMassIndex();
    }

    public void printCar() {
        if(next == null)
            System.out.print("--(" + cat.name + ")");
        else{
            System.out.print("--(" + cat.name + ")");
            next.printCar();
        }
    }
}
