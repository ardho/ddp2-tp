public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name; 
        this.weight = weight; 
        this.length = length; 
    }

    public double computeMassIndex() {
        return this.weight / (this.length * this.length * 0.0001); 
    }
}
