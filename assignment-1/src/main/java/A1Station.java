import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    /* incorrect input case*/
    public static void warn(){
        System.out.println(
            "Exception occurred, input format:\n"+
            "First line contain one integer N.\n" +
            "Next N lines contain string with 2 positive real number separated by comma.");
    }

	/* depart the train */
	public static void depart(TrainCar train, double value){
    	System.out.print("The train departs to Javari Park\n(LOCO)<");
        train.printCar();
    	System.out.printf("\nAverage mass index of all cats: %.2f\n" + 
        "In average, the cats in the train are " + category(value) + "\n", value);
	}

    /* category based on bmi */
	public static String category(double x){
		if(x < 18.5)
			return "*underweight*";
		if(x < 25)
			return "*normal*";
		if(x < 30)
			return "*overweight*";
		return "*obese*";
	}

    /* main program */
    public static void main(String[] args) {
     try{
        /* Variable & input stuff */
    	Scanner input  = new Scanner(System.in);
    	int n          = input.nextInt();
    	WildCat[] data = new WildCat[n];			
    	TrainCar[] car = new TrainCar[n];
    	int currentCar = 1;
    	boolean flag   = false;
        String input_data[]; 

    	for(int idx = 0; idx < n; idx++){
    		/* input format : <name>,<weight>,<length> */
    		input_data 		= input.next().split(",");

    		/* Create WildCat Object, assign it to data[idx] */ 
    		String name 	= input_data[0];
    		double weight 	= Double.parseDouble(input_data[1]);
    		double length 	= Double.parseDouble(input_data[2]);
    		data[idx] 		= new WildCat(name, weight, length);

            /* incorrect input case */
            if(weight <= 0 || length <= 0){
                warn();
                break;
            }  

			/* check if the total weight of all cars in the current 
			train has exceeded the threshold */
    		if(idx != 0 && !flag && car[idx-1].computeTotalWeight() + data[idx].weight + 20 >= THRESHOLD){
                /* Create car-idx object */
                car[idx] = new TrainCar(data[idx], car[idx-1]);
                currentCar++;
    			double average = car[idx].computeTotalMassIndex()/currentCar;

    			/* depart the train */
    			depart(car[idx], average);

    			/* empty the track */
                currentCar = 1;
                flag = true;

    		}
    		else{
    			/* No cars on the track, create car-1 */
    			if(idx == 0 || flag){
    				car[idx] = new TrainCar(data[idx]);
                    currentCar = 1;
                    flag = false;
    			}
    			else{
	    			/* Link car-idx with previous (idx-1) */
                    currentCar++;
	    			car[idx] = new TrainCar(data[idx], car[idx-1]);
				}
    		}

    		/* End of input case */
    		if(idx == n-1 && !flag){
    			double average = car[idx].computeTotalMassIndex()/currentCar;
    			/* depart the train immediately */
    			depart(car[idx], average);
    		}
    	}
     }
     catch(Exception e){
         warn();
     }
    }

}
