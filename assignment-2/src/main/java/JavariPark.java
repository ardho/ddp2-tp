import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;
import animal.Animal;
import cage.Cage;
import cage.CageArrangement;
import java.util.ArrayList;
import java.util.Scanner;

public class JavariPark{

	static Cat cat;
    static Hamster hamster;
    static Parrot parrot;
    static Eagle eagle;
    static Lion lion;

	static String[] animals = {"cat", "hamster", "parrot", "eagle", "lion"};
	static Cage[] cats, hamsters, parrots, eagles, lions;
	static Cage[][] cages = new Cage[5][]; // arrangement purpose, isinya hewan2
	static Scanner scan = new Scanner(System.in);

	public static void main(String[] args){
		System.out.println("Welcome to Javari Park!");
		inputAnimal();
		System.out.println("=============================================");
        startArrange();
		System.out.println("=============================================");
		while (touring()){
			CareTaker.back2office();
			System.out.println();
		}

	}

	public static void inputAnimal(){
		String[] curr_input, curr_obj;
		int n;

		System.out.println("Input the number of Animals");
		for(int i = 0; i < animals.length; i++){ // 0 = cat, 1 = hamster, 2 = parrot, 3 = eagle, 4 = lion
			System.out.printf("%s: ", animals[i]); 

			n = scan.nextInt();
			cages[i] = new Cage[n];
			if (animals[i].equals("cat"))
				cats = cages[i];
			else if (animals[i].equals("hamster"))
				hamsters = cages[i];
			else if (animals[i].equals("parrot"))
				parrots = cages[i];
			else if (animals[i].equals("eagle"))
				eagles = cages[i];
			else
				lions = cages[i];
			scan.nextLine();

			if (n == 0)
				continue;

			System.out.printf("Provide the information of %s(s):\n", animals[i]);
			curr_input = scan.nextLine().split(",");

			for(int j = 0; j < curr_input.length; j++){
				curr_obj = curr_input[j].split("\\|"); // name|length

				if (animals[i].equals("cat")){
					cat = new Cat(curr_obj[0], Integer.parseInt(curr_obj[1]));
					cats[j] = new Cage(cat);
					cages[i][j] = cats[j];
				}
				else if (animals[i].equals("hamster")){
					hamster = new Hamster(curr_obj[0], Integer.parseInt(curr_obj[1]));
					hamsters[j] = new Cage(hamster);
					cages[i][j] = hamsters[j];
				}
				else if (animals[i].equals("parrot")){
					parrot = new Parrot(curr_obj[0], Integer.parseInt(curr_obj[1]));
					parrots[j] = new Cage(parrot);
					cages[i][j] = parrots[j];
				}
				else if (animals[i].equals("eagle")){
					eagle = new Eagle(curr_obj[0], Integer.parseInt(curr_obj[1]));
					eagles[j] = new Cage(eagle);
					cages[i][j] = eagles[j];
				}
				else{ // lion
					lion = new Lion(curr_obj[0], Integer.parseInt(curr_obj[1]));
					lions[j] = new Cage(lion);
					cages[i][j] = lions[j];
				}
			}
		}
		System.out.println("Animal has been successfully recorded!\n");
	}

	private static void startArrange(){
		System.out.println("Cage arrangement:");
		for(int i = 0; i < cages.length; i++){
			if (cages[i].length > 0){
                System.out.printf("location: %s%n", cages[i][0].getLoc());
                
                /* Arrange (save it into perLevel*/
                ArrayList<Cage>[] perLevel = CageArrangement.arrangeCage(cages[i]);
                CageArrangement.printt(perLevel);

                /* Rearrange */
                System.out.println("After rearrangement...");
                ArrayList<Cage>[] newPerLevel = CageArrangement.rearrangementCage(perLevel);
                CageArrangement.printt(newPerLevel);
			}
		}

		for(int i = 0; i < cages.length; i++)
			System.out.printf("%s\t: %d%n", animals[i], cages[i].length);
		System.out.println();
	}

	private static boolean touring(){
		System.out.println("Which animal you want to visit?\n"
            + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: exit)");
		int inp = scan.nextInt(), temp;
		if (inp == 99){
			System.out.println("Thank you for visiting Javari Park!");
			return false;
		}
		else{
			scan.nextLine();
			if (inp == 1){
				CareTaker.askName("cat");
				Cat cat = findCat(scan.nextLine());
				if (cat != null){
					CareTaker.visiting(cat.getName(), "cat");
					System.out.println("1: Brush the fur 2: Cuddle");

                    temp = scan.nextInt();
                    if (temp == 1)
                        CareTaker.furCat(cat);
                    else if (temp == 2)
                        CareTaker.cuddlingCat(cat);
                    else
                        CareTaker.doNothing();
                } 
                else
                    CareTaker.notFound("cat");
			}

			else if (inp == 2){
				CareTaker.askName("hamster");
                Hamster hamster = findHamster(scan.nextLine());
                if (hamster != null){
                    CareTaker.visiting(hamster.getName(), "hamster");
                    System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                    temp = scan.nextInt();
                    if (temp == 1)
                        CareTaker.gnawHamster(hamster);
                    else if (temp == 2)
                        CareTaker.onWheelHamster(hamster);
                    else
                        CareTaker.doNothing();
                }
                else 
                	CareTaker.notFound("hamster");
			}

			else if (inp == 3){
				CareTaker.askName("parrot");
                Parrot parrot = findParrot(scan.nextLine());
                if (parrot != null){
                    CareTaker.visiting(parrot.getName(), "parrot");
                    System.out.println("1: Order to fly 2: Do conversation");
                    temp = scan.nextInt();
                    if (temp == 1)
                        CareTaker.flyParrot(parrot);
                    else if (temp == 2){
                        scan.nextLine();
                        System.out.print("You say: ");
                        String word = scan.nextLine();
                        CareTaker.repeatParrot(parrot, word);
                    }
                    else
                    	CareTaker.repeatParrot(parrot, "hm?");
                }
                else
                    CareTaker.notFound("parrot");
            }

            else if (inp == 4){
                CareTaker.askName("eagle");
                Eagle eagle = findEagle(scan.nextLine());
                if (eagle != null) {
                    CareTaker.visiting(eagle.getName(), "eagle");
                    System.out.println("1: Order to fly");
                    if (scan.nextInt() == 1)
                        CareTaker.flyEagle(eagle);
                    else
                    	CareTaker.doNothing();
                } 
                else
                	CareTaker.notFound("eagle");
            }

            else if (inp == 5){
            	CareTaker.askName("Lion");
                Lion lion = findLion(scan.nextLine());
                if (lion != null) {
                    CareTaker.visiting(lion.getName(), "lion");
                    System.out.println("1: see it hunting 2: Brush the mane 3: Disturb it");
                    temp = scan.nextInt();
                    if (temp == 1)
                        CareTaker.huntLion(lion);
                    else if (temp == 2)
                        CareTaker.brushLionMane(lion);
                    else if (temp == 3)
                        CareTaker.makeVoice(lion.getName(), lion.scream().toUpperCase());
                    else
                        CareTaker.doNothing();
                }
                else
                    CareTaker.notFound("lion");
            }
            
            else
            	CareTaker.doNothing();

			return true;
		}
	}
	

	public static Cat findCat(String name){
        for (int i = 0; i < cats.length; i++){
            if (cats[i].getAnimal().getName().equals(name)){
                return (Cat)cats[i].getAnimal();
            }
        }
        return null;
    }

    public static Hamster findHamster(String name){
        for (int i = 0; i < hamsters.length; i++){
            if (hamsters[i].getAnimal().getName().equals(name)){
                return (Hamster)hamsters[i].getAnimal();
            }
        }
        return null;
    }


    public static Parrot findParrot(String name){
        for (int i = 0; i < parrots.length; i++){
            if (parrots[i].getAnimal().getName().equals(name)){
                return (Parrot)parrots[i].getAnimal();
            }
        }
        return null;
    }

    public static Eagle findEagle(String name){
        for (int i = 0; i < eagles.length; i++){
            if (eagles[i].getAnimal().getName().equals(name)){
                return (Eagle)eagles[i].getAnimal();
            }
        }
        return null;
    }

    public static Lion findLion(String name){
        for (int i = 0; i < lions.length; i++){
            if (lions[i].getAnimal().getName().equals(name)){
                return (Lion)lions[i].getAnimal();
            }
        }
        return null;
    }



}