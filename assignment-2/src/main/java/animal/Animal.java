package animal;

public class Animal{

	protected String name;
	protected int length;
	protected boolean isWild; // 1 = wild, 0 otherwise

	public Animal(String name, int length, boolean x){
		this.name = name;
		this.length = length;
		this.isWild = x;
	}

	public String getName(){
		return this.name;
	}

	public int getLength(){
		return this.length;
	}

	public boolean getIsWild(){
		return this.isWild;
	}
}