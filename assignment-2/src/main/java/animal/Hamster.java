package animal;

public class Hamster extends Animal{

	public Hamster(String name, int length){
		super(name, length, false);
	}

    public String gnaw(){
        return "ngkkrit.. ngkkrrriiit";
    }

    public String onWheel(){
        return "trrr....trrr....";
    }
}