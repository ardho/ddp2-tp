package animal;

public class Lion extends Animal{

	public Lion(String name, int length){
		super(name, length, true);
	}

	public String hunt(){
		return "Err....";
	}

	public String scream(){
		return "Hauhhmm!";
	}
}