package javari.park;

import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;

/**
 * The interface describes expected behaviours for the attraction
 *
 * @author Yusuf T Ardho
 */
public class Attraction implements SelectedAttraction {

    private List<Animal> performers;
    private String name;
    private String type;

    public Attraction(String name, String type, ArrayList<Animal> performers) {
        this.name = name;
        this.type = type;
        this.performers = performers;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Animal> getPerformers() {
        return performers;
    }

    /**
     * Adds animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     *     performers, {@code false} otherwise
     */
    public boolean addPerformer(Animal performer) {
        performers.add(performer);
        return true;
    }
}