package javari.park;

import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;

/**
 * Reptillian Kingdom Class
 *
 * @author Yusuf T Ardho
 */
public class ReptillianKingdom extends Section {

    public ReptillianKingdom() {
        super("Reptillian Kingdom");
    }

    public Attraction menu() {
        while (true) {
            Scanner scan = new Scanner(System.in);

            System.out.println("--Reptillian Kingdom--");
            System.out.println("1. Snake");
            System.out.print("Please choose your preferred animals (type the number): ");
            String cmd = scan.nextLine();

            System.out.println("");
            if (cmd.equals("#")) 
                return null;

            ArrayList<Animal> performers = new ArrayList<Animal>();

            for (Animal item : animalList)
                if (item.isShowable()) 
                    performers.add(item);

            if (performers.size() == 0) {
                System.out.println("Unfortunately, no snake can perform any attraction," +
                        " please choose other animals");
                continue;
            }

            System.out.println("---Snake---");
            System.out.println("Attractions by Snake:");
            System.out.println("1. Dancing Animals");
            System.out.println("2. Passsionate coders");
            System.out.print("Please choose your preferred attractions (type the number): ");
            cmd = scan.nextLine();

            System.out.println("");
            if(cmd.equals("#")) 
                continue;

            int pick = Integer.parseInt(cmd);

            if (pick == 1)
                return new Attraction("Dancing Animals", "Snake", performers);
            else
                return new Attraction("Passionate Coders", "Snake", performers);
        }
    }

}