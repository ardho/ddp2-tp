package javari.park;

import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;

/**
 * World Of Aves Class
 *
 * @author Yusuf T Ardho
 */

public class WorldOfAves extends Section {

    public WorldOfAves() {
        super("World of Aves");
    }

    public Attraction menu() {
        while (true) {
            Scanner scan = new Scanner(System.in);

            System.out.println("\n--World of Aves--");
            System.out.println("1. Eagle");
            System.out.println("2. Parrot");
            System.out.print("Please choose your preferred animals (type the number): ");
            String cmd = scan.nextLine();

            System.out.println("");
            if (cmd.equals("#"))
                return null;

            int pick = Integer.parseInt(cmd);

            if (pick == 1) {
                ArrayList<Animal> performers = new ArrayList<Animal>();
                for (Animal item : animalList)
                    if ((item.getType().equalsIgnoreCase("Eagle")) && (item.isShowable()))
                        performers.add(item);

                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Eagle can perform any attraction," +
                            " please choose other animals");
                    continue;
                }

                System.out.println("---Eagle---");
                System.out.println("Attractions by Eagle:");
                System.out.println("1. Circles of Fire");
                System.out.print("Please choose your preferred attractions (type the number): ");
                cmd = scan.nextLine();

                System.out.println("");
                if(cmd.equals("#")) 
                    continue;

                int pick2 = Integer.parseInt(cmd);

                return new Attraction("Circles of Fire", "Eagle", performers);
            } 
            else {
                ArrayList<Animal> performers = new ArrayList<Animal>();
                for (Animal item : animalList)
                    if ((item.getType().equalsIgnoreCase("Parrot")) && (item.isShowable()))
                        performers.add(item);

                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Parrot can perform any attraction," +
                            " please choose other animals");
                    continue;
                }

                System.out.println("---Parrot---");
                System.out.println("Attractions by Parrot:");
                System.out.println("1. Dancing Animals");
                System.out.println("2. Counting Masters");
                System.out.print("Please choose your preferred attractions (type the number): ");
                cmd = scan.nextLine();

                System.out.println("");
                if(cmd.equals("#")) 
                    continue;

                int pick2 = Integer.parseInt(cmd);
                if (pick2 == 1)
                    return new Attraction("Dancing Animals", "Parrot", performers);
                else
                    return new Attraction("Counting Masters", "Parrot", performers);
            }
        }
    }

}