package javari.park;

import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.park.Attraction;

/**
 * Explore The Mammals Class
 *
 * @author Yusuf T Ardho
 */
public class ExploreTheMammals extends Section {

    public ExploreTheMammals() {
        super("Explore the Mammals");
    }

    public Attraction menu() {
        while (true) {
            Scanner scan = new Scanner(System.in);

            System.out.println("\n--Explore the Mammals--");
            System.out.println("1. Hamster");
            System.out.println("2. Lion");
            System.out.println("3. Cat");
            System.out.println("4. Whale");
            System.out.print("Please choose your preferred animals (type the number): ");
            String cmd = scan.nextLine();

            System.out.println("");
            if (cmd.equals("#")) 
                return null;

            int pick = Integer.parseInt(cmd);

            ArrayList<Animal> performers = new ArrayList<Animal>();

            if (pick == 1) {
                for (Animal item : animalList) 
                    if ((item.getType().equalsIgnoreCase("Hamster")) && (item.isShowable()))
                        performers.add(item);

                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Hamster can perform any attraction," +
                            " please choose other animals");
                    continue;
                }

                System.out.println("---Hamster---");
                System.out.println("Attractions by Hamster:");
                System.out.println("1. Dancing Animals");
                System.out.println("2. Counting Masters");
                System.out.println("3. Passionate Coders");
                System.out.print("Please choose your preferred attractions (type the number): ");
                cmd = scan.nextLine();

                System.out.println("");
                if (cmd.equals("#"))
                    continue;

                int pick2 = Integer.parseInt(cmd);

                if (pick2 == 1)
                    return new Attraction("Dancing Animals", "Hamster", performers);
                else if (pick2 == 2)
                    return new Attraction("Counting Masters", "Hamster", performers);
                else
                    return new Attraction("Passionate Coders", "Hamster", performers);

            } 
            else if (pick == 2) {
                for (Animal item : animalList) 
                    if ((item.getType().equalsIgnoreCase("Lion")) && (item.isShowable()))
                        performers.add(item);

                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Lion can perform any attraction," +
                            " please choose other animals");
                    continue;
                }

                System.out.println("---Lion---");
                System.out.println("Attractions by Lion:");
                System.out.println("1. Circles of Fire");
                System.out.print("Please choose your preferred attractions (type the number): ");
                cmd = scan.nextLine();

                System.out.println("");
                if (cmd.equals("#")) 
                    continue;

                int pick2 = Integer.parseInt(cmd);

                return new Attraction("Circles of Fire", "Lion", performers);
            } 
            else if (pick == 3) {
                for (Animal item : animalList) 
                    if ((item.getType().equalsIgnoreCase("Cat")) && (item.isShowable())) 
                        performers.add(item);

                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Cat can perform any attraction," +
                            " please choose other animals");
                    continue;
                }

                System.out.println("---Cat---");
                System.out.println("Attractions by Cat:");
                System.out.println("1. Dancing Animals");
                System.out.println("2. Passionate Coders");
                System.out.print("Please choose your preferred attractions (type the number): ");
                cmd = scan.nextLine();

                System.out.println("");
                if(cmd.equals("#")) 
                    continue;

                int pick2 = Integer.parseInt(cmd);

                if (pick2 == 1)
                    return new Attraction("Dancing Animals", "Cat", performers);
                else
                    return new Attraction("Passionate Coders", "Cat", performers);
            } 
            else {
                for (Animal item : animalList) 
                    if ((item.getType().equalsIgnoreCase("Whale")) && (item.isShowable()))
                        performers.add(item);

                if (performers.size() == 0) {
                    System.out.println("Unfortunately, no Whale can perform any attraction," +
                            " please choose other animals");
                    continue;
                }

                System.out.println("---Whale---");
                System.out.println("Attractions by Whale:");
                System.out.println("1. Circles of Fire");
                System.out.println("2. Counting Masters");
                System.out.print("Please choose your preferred attractions (type the number): ");
                cmd = scan.nextLine();

                System.out.println("");
                if(cmd.equals("#")) 
                    continue;

                int pick2 = Integer.parseInt(cmd);

                if (pick2 == 1)
                    return new Attraction("Circles of Fire", "Whale", performers);
                else
                    return new Attraction("Counting Masters", "Whale", performers);
            }
        }
    }

}