package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Body;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Lion;
import javari.animal.Mammals;
import javari.animal.Reptiles;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Yusuf T Ardho
 */
public class AnimalRecordsReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AnimalRecordsReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Counts the number of valid records from CSV file.
     *
     * @return
     */
    public long countValidRecords() {
        List<String> strList = this.getLines();
        int ans = 0;
        for (String item : strList) {
            String[] current = item.split(",");
            if (isValid(current)) 
                ans++;
        }
        return ans;
    }

    /**
     * Counts the number of invalid records from CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        List<String> strList = this.getLines();
        int ans = 0;
        for (String item : strList) {
            String[] current = item.split(",");
            if (!isValid(current)) 
                ans++;
        }
        return ans;
    }

    /**
     * getList method to get animals from the CSV file
     *
     * @return an arraylist of animals
     */
    public ArrayList<Animal> getList() {
        List<String> strList = this.getLines();
        ArrayList<Animal> animalList = new ArrayList<Animal>();
        for (String item : strList) {
            String[] current = item.split(",");

            if (!isValid(current)) 
                continue;

            int id = Integer.parseInt(current[0]);
            String type = current[1];
            String name = current[2];
            Gender gender = Gender.parseGender(current[3]);
            double length = Double.parseDouble(current[4]);
            double weight = Double.parseDouble(current[5]);
            String spec = current[6];
            Condition cond = Condition.parseCondition(current[7]);
            Animal sem;

            if (type.equalsIgnoreCase("Parrot") || type.equalsIgnoreCase("Eagle"))
                sem = new Aves(id, type, name, gender, length, weight, cond, spec); 
            else if (type.equalsIgnoreCase("Snake"))
                sem = new Reptiles(id, type, name, gender, length, weight, cond, spec);
            else if (type.equalsIgnoreCase("Lion"))
                sem = new Lion(id, type, name, gender, length, weight, cond, spec);
            else
                sem = new Mammals(id, type, name, gender, length, weight, cond, spec);

            animalList.add(sem);
        }

        return animalList;
    }

    /**
     * isValid method to check from animals
     *
     * @param sample animal in array
     * @return boolean, true if sample data is valid
     */
    public boolean isValid(String[] sample) {
        String[] animals = {"Hamster", "Eagle", "Whale", "Cat", "Lion",
                            "Snake", "Parrot"};

        boolean flag = false;

        for (int i = 0; i < 7; i++)
            if (animals[i].equalsIgnoreCase(sample[1])) 
                flag = true;

        if (!flag) 
            return false;

        if (!(sample[3].equalsIgnoreCase("male") || sample[3].equalsIgnoreCase("female")))
            return false;

        if (!(sample[7].equalsIgnoreCase("not healthy") || sample[7].equalsIgnoreCase("healthy")))
            return false;

        return true;
    }

}
