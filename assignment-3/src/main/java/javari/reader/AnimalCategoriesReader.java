package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Yusuf T Ardho
 */
public class AnimalCategoriesReader extends CsvReader {

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public AnimalCategoriesReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Counts the number of valid records from CSV file.
     *
     * @return
     */
    public long countValidRecords() {
        List<String> strList = this.getLines();
        String[] mamList = {"Hamster", "Lion", "Cat", "Whale"};
        String[] avList = {"Parrot", "Eagle"};
        String[] repLis = {"snake"};

        long ans = 0;
        int flag_mam = 1, flag_av = 1, flag_rep = 1;

        for (String item : strList) {
            String[] current = item.split(",");
            if (current[1].equalsIgnoreCase("mammals")) {
                boolean sem = false;
                for (int i = 0; i < 4; i++)
                    if (current[0].equalsIgnoreCase(mamList[i])) 
                        sem = true;
                if (!sem) 
                    flag_mam = 0;
            }
            if (current[1].equalsIgnoreCase("aves")) {
                boolean sem = false;
                for (int i = 0; i < 2; i++)
                    if (current[0].equalsIgnoreCase(avList[i])) 
                        sem = true;
                if (!sem) 
                    flag_av = 0;
            }
            if (current[1].equalsIgnoreCase("reptiles")) {
                boolean sem = false;
                if (current[0].equalsIgnoreCase(repLis[0])) 
                    sem = true;
                if (!sem) 
                    flag_rep = 0;
            }
        }
        
        return flag_mam + flag_av + flag_rep;
    }

    /**
     * Counts the number of invalid records from CSV file.
     *
     * @return
     */
    public long countInvalidRecords() {
        long ans = 3 - countValidRecords();
        List<String> strList = this.getLines();
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (!anRec[1].equalsIgnoreCase("mammals") &&
                !anRec[1].equalsIgnoreCase("aves") &&
                !anRec[1].equalsIgnoreCase("reptiles"))
                    ans++;
        }

        return ans;
    }

    /**
     * Counts the number of valid records from CSV file.
     *
     * @return
     */
    public long countValidSections() {
        List<String> strList = this.getLines();

        long ans = 0;
        int flag_mam = 1, flag_av = 1, flag_rep = 1;

        for (String item : strList) {
            String[] anRec = item.split(",");
            if (anRec[2].equalsIgnoreCase("Explore the Mammals") &&
                !anRec[1].equalsIgnoreCase("mammals"))
                    flag_mam = 0;

            if (anRec[2].equalsIgnoreCase("World of Aves") &&
                !anRec[1].equalsIgnoreCase("aves"))
                    flag_av = 0;

            if (anRec[2].equalsIgnoreCase("Reptillian Kingdom") &&
                !anRec[1].equalsIgnoreCase("reptiles"))
                    flag_rep = 0;
        }

        return flag_mam + flag_av + flag_rep;
    }

    /**
     * Counts the number of invalid records from CSV file.
     *
     * @return
     */
    public long countInvalidSections() {
        long ans = 3 - countValidSections();
        List<String> strList = this.getLines();
        for (String item : strList) {
            String[] anRec = item.split(",");
            if (!anRec[2].equalsIgnoreCase("Explore the Mammals") &&
                !anRec[2].equalsIgnoreCase("World of Aves") &&
                !anRec[2].equalsIgnoreCase("Reptillian Kingdom"))
                    ans++;
        }
        
        return ans;
    }
}
