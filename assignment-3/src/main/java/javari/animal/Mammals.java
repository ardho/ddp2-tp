package javari.animal;

/**
 * This class represents common attributes and behaviours found in all mammals
 * in Javari Park.
 *
 * @author Yusuf T Ardho
 */
public class Mammals extends Animal {

    private String specificCondition;

    /**
     * Constructs an instance of {@code Mammals}.
     */
    public Mammals(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, String specificCondition) {
        super(id, type, name, gender, length, weight, condition);
        this.specificCondition = specificCondition;
    }

    /**
     * to know whether an animal is able to perform or not.
     *
     * @return
     */
    protected boolean specificCondition() {
        return !(specificCondition.equalsIgnoreCase("Pregnant"));
    }
}