package board;

import card.Card;

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.Timer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import java.awt.Image;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.BorderLayout;

/**
 * Class of Board Game
 *
 * @author Yusuf T Ardho
 */

public class Board extends JFrame {

    private static final String path = "/home/rdo/ddp2-tp/assignment-4/src/img/";
    private static final int ALL = 18;
    private final ArrayList<Card> cardList;
    private final Timer timer;
    private Card firstCard, secondCard;
    private JFrame mainFrame;
    private JPanel topPanel, botPanel;
    private JLabel tryLabel, hintLabel, copyrightLabel;
    private JButton resetButton, quitButton, hintButton;
    private int tries = 0, foundPairCards = 0, hintUses = -1;

    /**
     * Constructor of Board Game
     */

    public Board() {
        this.cardList = new ArrayList<>();
        ArrayList<Integer> cardIDs = new ArrayList<>();

        for (int i = 1; i < ALL; i++) {
            if (i == 2) {
                cardIDs.add(i); cardIDs.add(i);
            }
            cardIDs.add(i); cardIDs.add(i);
		}

        for (int i: cardIDs) {
            ImageIcon img = new ImageIcon(path + "cat" + i + ".png");
            Card card = new Card(i, img);
            card.getButton().addActionListener(actionPerformed -> openCard(card));
            this.cardList.add(card);
		}

        timer = new Timer(500, actionPerformed -> checkingPair());
        timer.setRepeats(false);
        buildBoardWindow();
    }

    /**
     * Method to open the selected card.
     * @param selectedCard card that is selected
     */

    private void openCard(Card selectedCard) {
    	// choose first card
        if (firstCard == null && secondCard == null) {
            firstCard = selectedCard;
            firstCard.faceUp();
        }

        // choose the second card
        if (firstCard != null && firstCard != selectedCard && secondCard == null) {
            secondCard = selectedCard;
            secondCard.faceUp();
            timer.start();
        }
    }

    /**
     * Method to check the selected of pair card whether is match / not
     */

    private void checkingPair() {
        if (firstCard.getId() == secondCard.getId()) {
            disableButton();
            if (foundPairCards == ALL) {
            	JLabel messageLabel = new JLabel("Congratulations! You won!");
				messageLabel.setHorizontalAlignment(SwingConstants.CENTER);
                Object[] choices = {"Play Again!", "Quit"};
                int option = JOptionPane.showOptionDialog(null, messageLabel,
                        "Remember the cat", JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.PLAIN_MESSAGE, null, choices, choices[1]);
                if (option == 0) {
                    restart();
                } 
                else {
                    System.exit(0);
                }
            }
        } 
        else {
            firstCard.faceDown();
            secondCard.faceDown();
            setTries(getTries() + 1);
        }
        firstCard = null;
        secondCard = null;
    }

    /**
     * Method to disable and flags the card button
     */

    private void disableButton() {
        firstCard.getButton().setEnabled(false);
        secondCard.getButton().setEnabled(false);
        firstCard.setMatched(true);
        secondCard.setMatched(true);
        setFoundPairCards(getFoundPairCards() + 1);
    }

    /**
     * Method to build the window of the game
     */

    private void buildBoardWindow() {
        buildTopPanel();
        buildBotPanel();

        mainFrame = new JFrame("Remember the cat");
        mainFrame.setLayout(new BorderLayout());

        mainFrame.add(topPanel, BorderLayout.CENTER);
        mainFrame.add(botPanel, BorderLayout.SOUTH);

        mainFrame.setPreferredSize(new Dimension(800, 800));
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setLocation(550, 125);
        mainFrame.pack();

        mainFrame.setVisible(true);
        mainFrame.setResizable(true);

        Timer waitTime = new Timer(100, actionPerformed -> faceUpAllCards());
        waitTime.setRepeats(false);
        waitTime.start();
    }

    /**
     * Method to build the top panel
     */

    private void buildTopPanel() {
        topPanel = new JPanel(new GridLayout(6, 6));

		Collections.shuffle(cardList);

        for (Card card: cardList) {
        	card.setMatched(false);
        	card.getButton().setEnabled(true);
        	card.faceDown();
            topPanel.add(card.getButton());
        }
    }

    /**
     * Method to build the bot panel
     */

    private void buildBotPanel() {
        botPanel = new JPanel(new GridLayout(2,3));

        hintButton = new JButton("Hint");
        resetButton = new JButton("Restart");
        quitButton = new JButton("Quit");
        tryLabel = new JLabel("Number of Tries: " + tries);
        hintLabel = new JLabel("Number of Hint Uses: " + hintUses);
        copyrightLabel = new JLabel("(Copyright 2018 by rd0)");

        hintButton.addActionListener(actionPerformed -> faceUpAllCards());
        resetButton.addActionListener(actionPerformed -> restart());
        quitButton.addActionListener(actionPerformed -> exitGame());
        tryLabel.setHorizontalAlignment(SwingConstants.CENTER);
        hintLabel.setHorizontalAlignment(SwingConstants.CENTER);
        copyrightLabel.setHorizontalAlignment(SwingConstants.CENTER);

        botPanel.add(hintButton);
        botPanel.add(resetButton);
        botPanel.add(quitButton);
        botPanel.add(tryLabel);
        botPanel.add(hintLabel);
        botPanel.add(copyrightLabel);
    }

    /**
     * Accessor to get the number of wrong pairing card
     * @return number of tries
     */

    public int getTries() {
        return this.tries;
    }

    /**
     * Accessor to get the number of using hint
     * @return number of hint uses
     */

    public int getHintUses() {
        return this.hintUses;
    }

    /**
     * Accessor to get the number of success pairing cards
     * @return number of success pairing the cards
     */

    public int getFoundPairCards() {
        return this.foundPairCards;
    }

    /**
     * Mutator to set the number of success pairing card
     * @param x that will be assign to foundPairCards 
     */

    public void setFoundPairCards(int x) {
        this.foundPairCards = x;
    }

    /**
     * Mutator to set the number of fail pairing card
     * @param x that will be assign to this.tries 
     */

    public void setTries(int x) {
        tryLabel.setText("Number of Tries: " + x);
        this.tries = x;
    }

    /**
     * Mutator to set the number of using hint
     * @param x that will be assign to hintUses 
     */

    public void setHintUses(int x) {
        hintLabel.setText("Number of Hint Uses: " + (x == -1 ? 0 : x));
        this.hintUses = x == -1 ? -1 : x;
    }

    /**
     * Method to restart the game
     */

    private void restart() {
        mainFrame.remove(topPanel);
        buildTopPanel();
        mainFrame.add(topPanel, BorderLayout.CENTER);
        setTries(0);
        setHintUses(-1);
        setFoundPairCards(0);
        faceUpAllCards();
    }

    /**
     * Method to face up all card in specific time
     */

    private void faceUpAllCards() {
        setHintUses(getHintUses);
        for (Card card: cardList) {
            card.faceUp();
        }

        topPanel.updateUI();

        Timer openTime = new Timer(2500, actionPerformed -> faceDownAllCards());
        openTime.setRepeats(false);
        openTime.start();
    }

    /**
     * Method to face down all card, except if the card already matched
     */

    private void faceDownAllCards() {
        for (Card card: cardList) {
        	if (card.getIsMatched())
        		continue;
            card.faceDown();
        }
        topPanel.updateUI();
    }

    /**
     * Method to exit the Game (Program)
     */

    private void exitGame() {
        if (JOptionPane.showConfirmDialog(null, "Are you sure ?",
                "Remember The Cat", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }
}